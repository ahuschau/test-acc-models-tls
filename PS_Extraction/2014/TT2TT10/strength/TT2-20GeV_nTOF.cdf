(* Content-type: application/vnd.wolfram.cdf.text *)

(*** Wolfram CDF File ***)
(* http://www.wolfram.com/cdf *)

(* CreatedBy='Mathematica 9.0' *)

(*************************************************************************)
(*                                                                       *)
(*  The Mathematica License under which this file was created prohibits  *)
(*  restricting third parties in receipt of this file from republishing  *)
(*  or redistributing it by any means, including but not limited to      *)
(*  rights management or terms of use, without the express consent of    *)
(*  Wolfram Research, Inc. For additional information concerning CDF     *)
(*  licensing and redistribution see:                                    *)
(*                                                                       *)
(*        www.wolfram.com/cdf/adopting-cdf/licensing-options.html        *)
(*                                                                       *)
(*************************************************************************)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[      1063,         20]
NotebookDataLength[     55296,       1766]
NotebookOptionsPosition[     50319,       1571]
NotebookOutlinePosition[     50785,       1589]
CellTagsIndexPosition[     50742,       1586]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{
     RowBox[{"Updated", " ", "in", " ", 
      RowBox[{"July", "'"}], "06", " ", "by", " ", "Elena", " ", 
      "Benedetto"}], " ", "\[Rule]", " ", 
     RowBox[{
     "New", " ", "quadrupoles", " ", "and", " ", "powering", " ", "scheme", 
      " ", "in", " ", "order", " ", "to", " ", "make", " ", "a", " ", "low", 
      " ", "beta", " ", "insertion", " ", "and", " ", "accomodate", " ", "a", 
      " ", "Ion", " ", "stripper", " ", "foil", " ", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{
         RowBox[{"\[Rule]", " ", 
          RowBox[{"M", ".", "Martini"}]}], ",", " ", 
         RowBox[{"see", " ", "his", " ", "presentation", " ", 
          RowBox[{
           RowBox[{"20", "/", "10"}], "/", "05"}]}]}], ")"}], ".", 
       "\[IndentingNewLine]", "\[IndentingNewLine]", "Quadrupoles"}], " ", 
      "QDE210", " ", "and", " ", "QF0215", " ", "disconnected", " ", "from", 
      " ", "the", " ", "strings", " ", "and", " ", "powered", " ", 
      RowBox[{"separately", ".", " ", "Now"}], " ", "the", " ", "defocusing", 
      " ", "string", " ", "start", " ", "with", " ", "QDE240", " ", "and", 
      " ", "the", " ", "focusing", " ", "with", " ", 
      RowBox[{"QFO225", ".", " ", "The"}], " ", "magnet", " ", "QF0215", " ", 
      RowBox[{"(", 
       RowBox[{"was", " ", "0.8", "m", " ", "long"}], ")"}], " ", "has", " ", 
      "been", " ", "replaced", " ", "by", " ", "a", " ", "\"\<long\>\"", " ", 
      "one", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"1.2", "m"}], ")"}], ".", "\[IndentingNewLine]", 
       RowBox[{"!!", 
        RowBox[{"!", 
         RowBox[{"!", 
          RowBox[{"!", 
           RowBox[{"!", 
            RowBox[{"!", 
             RowBox[{"!", 
              RowBox[{"!", 
               RowBox[{"!", 
                RowBox[{"!", 
                 RowBox[{"!", 
                  RowBox[{"!", 
                   RowBox[{"!", 
                    RowBox[{"!", 
                    RowBox[{"!", 
                    RowBox[{"!", 
                    RowBox[{"!", 
                    RowBox[{"!", 
                    RowBox[{"!", 
                    RowBox[{"!", 
                    RowBox[{"!", 
                    RowBox[{"!", 
                    RowBox[{"!", 
                    RowBox[{"!", 
                    RowBox[{"!", 
                    RowBox[{"!", 
                    RowBox[{"!", 
                    RowBox[{"!", 
                    RowBox[{"!", 
                    RowBox[{"!", 
                    RowBox[{"!", " ", 
                    RowBox[{
                    "Additional", " ", "quadrupoles", " ", "have", " ", 
                    "been", " ", 
                    RowBox[{
                    "installed", ":", " ", 
                    "QDE163"}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]\
}]}]}]}]}]}]}]}]}]}]}], ",", " ", "QDE207", ",", " ", "QDE213", ",", " ", 
    RowBox[{"QDE217", 
     RowBox[{"(", 
      RowBox[{"\[Rule]", 
       RowBox[{"old", " ", "QFO215"}]}], ")"}], " ", "but", " ", "are", " ", 
     "NOT", " ", "YET", " ", "IN", " ", "THE", " ", 
     RowBox[{"NOTEBOOK", " ", ".", " ", 
      RowBox[{"!!", 
       RowBox[{"!", 
        RowBox[{"!", 
         RowBox[{"!", 
          RowBox[{"!", 
           RowBox[{"!", 
            RowBox[{"!", 
             RowBox[{"!", 
              RowBox[{"!", 
               RowBox[{"!", 
                RowBox[{"!", 
                 RowBox[{"!", 
                  RowBox[{"!", 
                   RowBox[{"!", 
                    RowBox[{"!", 
                    RowBox[{"!", 
                    RowBox[{"!!", 
                    RowBox[{"!!", 
                    RowBox[{"!", "\[IndentingNewLine]", "\[IndentingNewLine]", 
                    RowBox[{
                    "This", " ", "file", " ", "is", " ", "for", " ", "nTOF", 
                    " ", "at", " ", "20", 
                    RowBox[{
                    "GeV", "/", 
                    "c"}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}], 
   "\[IndentingNewLine]", "\[IndentingNewLine]", "  ", "*)"}], 
  "\[IndentingNewLine]", "\[IndentingNewLine]", " "}]], "Input",
 CellChangeTimes->{{3.4522363393791265`*^9, 3.4522364373934965`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"Off", "[", 
   RowBox[{"General", "::", "spell"}], "]"}], ";", 
  RowBox[{"Off", "[", 
   RowBox[{"General", "::", "spell1"}], "]"}], ";"}]], "Input"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"<<", "Statistics`DescriptiveStatistics`"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"General", "::", "\<\"obspkg\"\>"}], ":", 
  " ", "\<\"\\!\\(\\\"Statistics`DescriptiveStatistics`\\\"\\) is now \
obsolete. The legacy version being loaded may conflict with current \
Mathematica functionality. See the Compatibility Guide for updating \
information. \\!\\(\\*ButtonBox[\\\"\[RightSkeleton]\\\", \
ButtonStyle->\\\"Link\\\", ButtonFrame->None, \
ButtonData:>\\\"paclet:Compatibility/Tutorials/Statistics/\
DescriptiveStatistics\\\", ButtonNote -> \\\"General::obspkg\\\"]\\)\"\>"}]], \
"Message", "MSG",
 CellChangeTimes->{3.452233826942536*^9, 3.452234353779542*^9, 
  3.4522367655142965`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"momentum", "=", "20.0"}]], "Input",
 CellChangeTimes->{{3.452233914566414*^9, 3.452233914691413*^9}}],

Cell[BoxData["20.`"], "Output",
 CellChangeTimes->{3.4522338271144085`*^9, 3.452234353779542*^9, 
  3.4522367655299215`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"B\[Rho]", "=", 
  RowBox[{"3.3356", " ", "momentum"}]}]], "Input"],

Cell[BoxData["66.712`"], "Output",
 CellChangeTimes->{3.4522338271300335`*^9, 3.452234353795167*^9, 
  3.4522367655299215`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[StyleBox["Input values (either currents or strength)",
 FontFamily->"Arial",
 FontSize->14,
 FontSlant->"Italic"]], "Subsubtitle"],

Cell[CellGroupData[{

Cell["Input strengths", "Section"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"k1QFO105", "=", "0.20394437"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"k1QDE120", "=", " ", 
   RowBox[{"-", "0.08760635913"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"k1QFO135", "=", "0.07869850886"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"k1QDE150", "=", " ", 
   RowBox[{"-", "0.06954970925"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"k1QFO165", "=", "0.07263829231"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"k1QDE180", "=", 
   RowBox[{"-", "0.07244909"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"k1QFO205", "=", "   ", "0.06097295"}], ";"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"k1QDE210", "=", "0.1042264261"}], ";"}], "    ", 
  RowBox[{"(*", " ", 
   RowBox[{"there", " ", "is", " ", "a", " ", "change", " ", "of", " ", 
    RowBox[{
     RowBox[{"sign", "!!"}], "!"}]}], "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"k1QDE240S", "=", 
    RowBox[{"0.1044314276", "/", "1.001966885636"}]}], ";"}], 
  "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{"k1QDE220S", "=", 
     RowBox[{"k1QDE240S", "*", "1.001966885636"}]}], ";"}], " ", "*)"}], 
  "\[IndentingNewLine]", "\[IndentingNewLine]", 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"k1QFO215", "=", "0.07121418128"}], ";"}], "  ", 
  RowBox[{"(*", " ", 
   RowBox[{"\"\<long\>\"", " ", "1.2", "m"}], "  ", 
   "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"k1QFO225S", "=", "0.1068212719"}], ";"}], "\[IndentingNewLine]", 
  "\[IndentingNewLine]", "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"k1QFO375", "=", " ", "0.1321434752"}], ";"}], "  ", 
  "\[IndentingNewLine]", "\[IndentingNewLine]", "\n", 
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{"!", 
     RowBox[{"additional", " ", "quadrupoles", " ", "QDE163"}]}], ",", " ", 
    "QDE207", ",", " ", "QDE213", ",", " ", 
    RowBox[{
     RowBox[{"QDE217", 
      RowBox[{"(", 
       RowBox[{"\[Rule]", 
        RowBox[{"old", " ", "QFO215"}]}], ")"}]}], " ", ":", " ", 
     RowBox[{
     "have", " ", "all", " ", "zero", " ", "strenght", " ", "for", " ", "the",
       " ", "proton", " ", "at", " ", "26", "GeV", " ", "\[IndentingNewLine]",
       "NOT", " ", "YET", " ", "IN", " ", "THE", " ", 
      RowBox[{
       RowBox[{"NOTEBOOK", "!!"}], "!!"}]}]}]}], " ", "*)"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]"}], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Input currents ", "Section"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"IQFO105", "=", "408.2052210745936"}], ";"}], "\n", 
 RowBox[{
  RowBox[{"IQDE120", "=", " ", 
   RowBox[{"-", "136.3833557048152"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{"IQFO135", "=", "122.58330746237253"}], ";"}], "\n", 
 RowBox[{
  RowBox[{"IQDE150", "=", 
   RowBox[{"-", "108.3950159259495"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{"IQFO165", "=", "113.18678929895634"}], ";"}], "\n", 
 RowBox[{
  RowBox[{"IQDE180", "=", 
   RowBox[{"-", "112.8933090285222"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{
   RowBox[{"IQFO205", "=", "95.0777856323922"}], ";"}], 
  "\[IndentingNewLine]"}], "\n", 
 RowBox[{
  RowBox[{
   RowBox[{"IQDE210", "=", "159.8711902847877"}], ";"}], " ", 
  RowBox[{"(*", " ", 
   RowBox[{
   "disconnected", " ", "from", " ", "the", " ", "defocusing", " ", 
    "string"}], " ", "*)"}]}], "\n", 
 RowBox[{
  RowBox[{
   RowBox[{"IQDE240S", "=", "159.87119034520398"}], ";"}], "  ", 
  RowBox[{"(*", " ", 
   RowBox[{"start", " ", "of", " ", "the", " ", "defocusing", " ", "string"}],
    " ", "*)"}], "                      ", "\n", 
  RowBox[{"(*", " ", 
   RowBox[{"IQDE220s", "=", " ", 
    RowBox[{"IQDE240", "*", 
     RowBox[{"1.001966885636", "  ", "!!"}], 
     RowBox[{"!", " ", 
      RowBox[{"ele", "-", 
       RowBox[{
        RowBox[{"Nov", "'"}], "06"}]}]}]}]}], " ", "*)"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"IQFO215", "=", "110.97760445157282"}], ";"}], 
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{
    "disconnected", " ", "from", " ", "the", " ", "defocusing", " ", "string",
      " ", "and", " ", 
     RowBox[{"changed", ":", " ", 
      RowBox[{
       RowBox[{"since", " ", "now", " ", "is", " ", "1.2", "m", " ", "long"}],
        " ", "-", 
       RowBox[{"it", " ", "was", " ", "0.8", "m"}], "-", " ", 
       RowBox[{
       "to", " ", "get", " ", "the", " ", "same", " ", "field", " ", "the", 
        " ", "current", " ", "should", " ", "be", " ", 
        RowBox[{"0.8", "/", "1.2"}]}]}]}]}], "=", " ", 
    RowBox[{
     RowBox[{
      RowBox[{
       RowBox[{"2", "/", "3"}], " ", "the", " ", "previous", " ", "value"}], 
      "..."}], "apart", " ", "from", " ", "small", " ", "differences", " ", 
     "in", " ", "the", " ", "calibration", " ", "curve"}]}], " ", "*)"}], 
  "\[IndentingNewLine]"}], "\n", 
 RowBox[{
  RowBox[{
   RowBox[{"IQFO225S", "=", "162.95126731587646"}], ";"}], "  ", 
  RowBox[{"(*", " ", 
   RowBox[{"start", " ", "of", " ", "the", " ", "defocusing", " ", "string"}],
    " ", "*)"}], "\[IndentingNewLine]"}], "\n", 
 RowBox[{
  RowBox[{"IQFO375", "=", "201.43729987133216"}], ";"}]}], "Input",
 CellChangeTimes->{{3.452236537689088*^9, 3.4522366442658486`*^9}, {
  3.452236840810208*^9, 3.452236914168644*^9}}]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Type QF101 (QFO105)", "Subsubtitle"],

Cell[CellGroupData[{

Cell["QF101: MAD modelling", "Section"],

Cell["QFO105,    K1= +.0097608*IQFO105/MOMENTUM", "Text"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"LQF101", "=", "0.645"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"gLQFO105", "=", 
   RowBox[{"k1QFO105", " ", "LQF101", " ", "B\[Rho]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"IQFO105MAD", "=", 
   FractionBox[
    RowBox[{"k1QFO105", " ", "momentum"}], "0.0097608"]}], ";"}]}], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell["QF101: The two 2nd order polynomials p1QF101 and p2QF101", "Section"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"p1QF101", "[", "i_", "]"}], "=", 
    RowBox[{
     RowBox[{"-", "0.004"}], "+", 
     RowBox[{"0.021749", " ", "i"}], "-", 
     RowBox[{"0.000000506", " ", 
      SuperscriptBox["i", "2"]}]}]}], ";"}], " ", 
  RowBox[{"(*", 
   RowBox[{
    RowBox[{"ISR", "-", 
     RowBox[{
      RowBox[{"MA", "/", "KB"}], "/", "rh"}]}], ",", " ", 
    RowBox[{"13", " ", "January", " ", "1975"}]}], 
   "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"p2QF101", "[", "i_", "]"}], "=", 
    RowBox[{"0.006", "+", 
     RowBox[{"0.021506", " ", "i"}], "-", 
     RowBox[{"0.000000141", " ", 
      SuperscriptBox["i", "2"]}]}]}], ";"}], " ", 
  RowBox[{"(*", 
   RowBox[{
    RowBox[{"ISR", "-", 
     RowBox[{
      RowBox[{"MA", "/", "KB"}], "/", "rh"}]}], ",", " ", 
    RowBox[{"addendum", " ", "21", " ", "July", " ", "1975"}]}], 
   "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{"i", " ", "/.", " ", 
  RowBox[{"Part", "[", 
   RowBox[{
    RowBox[{"Part", "[", 
     RowBox[{
      RowBox[{"NSolve", "[", 
       RowBox[{
        RowBox[{
         RowBox[{"p1QF101", "[", "i", "]"}], "\[Equal]", "gLQFO105"}], ",", 
        "i"}], "]"}], ",", "1"}], "]"}], ",", "1"}], "]"}]}]}], "Input"],

Cell[BoxData["407.5411596188908`"], "Output",
 CellChangeTimes->{3.4522338272237825`*^9, 3.452234353810792*^9, 
  3.452236765561171*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"IQFO105a", "=", "%"}]], "Input"],

Cell[BoxData["407.5411596188908`"], "Output",
 CellChangeTimes->{3.452233827239407*^9, 3.4522343538264165`*^9, 
  3.452236765561171*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"i", " ", "/.", " ", 
  RowBox[{"Part", "[", 
   RowBox[{
    RowBox[{"Part", "[", 
     RowBox[{
      RowBox[{"NSolve", "[", 
       RowBox[{
        RowBox[{
         RowBox[{"p2QF101", "[", "i", "]"}], "\[Equal]", "gLQFO105"}], ",", 
        "i"}], "]"}], ",", "1"}], "]"}], ",", "1"}], "]"}]}]], "Input"],

Cell[BoxData["408.8692825302964`"], "Output",
 CellChangeTimes->{3.452233827239407*^9, 3.4522343538264165`*^9, 
  3.4522367655767956`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"IQFO105b", "=", "%"}]], "Input"],

Cell[BoxData["408.8692825302964`"], "Output",
 CellChangeTimes->{3.4522338272550316`*^9, 3.4522343538420415`*^9, 
  3.4522367655767956`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"IQFO105k", "=", 
  RowBox[{"Mean", "[", 
   RowBox[{"{", 
    RowBox[{"IQFO105a", ",", "IQFO105b"}], "}"}], "]"}]}]], "Input"],

Cell[BoxData["408.2052210745936`"], "Output",
 CellChangeTimes->{3.4522338272706566`*^9, 3.4522343538420415`*^9, 
  3.4522367655924206`*^9}]
}, Open  ]]
}, Open  ]]
}, Closed]],

Cell[CellGroupData[{

Cell["\<\
Type QFL (QDE120, QFO135, QDE150, QFO165, QDE180, QFO205,QFO215)\
\>", "Subsubtitle"],

Cell[CellGroupData[{

Cell["QFL: MAD modelling", "Section"],

Cell["", "Text"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"LQFL", "=", "1.2"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"gLQDE120", "=", 
   RowBox[{"k1QDE120", " ", "LQFL", " ", "B\[Rho]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"gLQFO135", "=", 
   RowBox[{"k1QFO135", " ", "LQFL", " ", "B\[Rho]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"gLQDE150", "=", 
   RowBox[{"k1QDE150", " ", "LQFL", " ", "B\[Rho]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"gLQFO165", "=", 
   RowBox[{"k1QFO165", " ", "LQFL", " ", "B\[Rho]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"gLQDE180", "=", 
   RowBox[{"k1QDE180", " ", "LQFL", " ", "B\[Rho]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"gLQFO205", "=", 
   RowBox[{"k1QFO205", " ", "LQFL", " ", "B\[Rho]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"gLQFO215", "=", 
    RowBox[{"k1QFO215", " ", "LQFL", " ", "B\[Rho]"}]}], ";"}], 
  "\[IndentingNewLine]", "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"IQDE120MAD", "=", 
   FractionBox[
    RowBox[{"k1QDE120", " ", "momentum"}], "0.0128163"]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"IQFO135MAD", "=", 
   FractionBox[
    RowBox[{"k1QFO135", " ", "momentum"}], "0.0128163"]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"IQDE150MAD", "=", 
   FractionBox[
    RowBox[{"k1QDE150", " ", "momentum"}], "0.0128163"]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"IQFO165MAD", "=", 
   FractionBox[
    RowBox[{"k1QFO165", " ", "momentum"}], "0.0128163"]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"IQDE180MAD", "=", 
   FractionBox[
    RowBox[{"k1QDE180", " ", "momentum"}], "0.0128163"]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"IQFO205MAD", "=", 
   FractionBox[
    RowBox[{"k1QFO205", " ", "momentum"}], "0.0128163"]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"IQFO215MAD", "=", 
    FractionBox[
     RowBox[{"k1QFO215", " ", "momentum"}], "0.0128163"]}], ";"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]"}], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell["QFL (Q120B):  7th order polynomials p1QFL ", "Section"],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"p1QFL", "[", "i_", "]"}], "=", 
    RowBox[{
     RowBox[{"0.5123159", " ", 
      SuperscriptBox["10", 
       RowBox[{"-", "1"}]], " ", "i"}], "+", 
     RowBox[{"0.132954", " ", 
      SuperscriptBox["10", 
       RowBox[{"-", "7"}]], " ", 
      SuperscriptBox["i", "3"]}], "-", 
     RowBox[{"0.1635244", " ", 
      SuperscriptBox["10", 
       RowBox[{"-", "12"}]], " ", 
      SuperscriptBox["i", "5"]}], "+", 
     RowBox[{"0.1443575", " ", 
      SuperscriptBox["10", 
       RowBox[{"-", "18"}]], " ", 
      SuperscriptBox["i", "7"]}]}]}], ";"}], " "}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"res", "=", 
   RowBox[{"NSolve", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"p1QFL", "[", "i", "]"}], "\[Equal]", "gLQDE120"}], ",", "i"}], 
    "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"(", 
    RowBox[{"i", "/.", "res"}], ")"}], "[", 
   RowBox[{"[", 
    RowBox[{
     RowBox[{"Position", "[", 
      RowBox[{
       RowBox[{
        RowBox[{
         RowBox[{"Im", "[", "#", "]"}], "&"}], "/@", 
        RowBox[{"(", 
         RowBox[{"i", "/.", "res"}], ")"}]}], ",", "0"}], "]"}], "[", 
     RowBox[{"[", 
      RowBox[{"1", ",", "1"}], "]"}], "]"}], "]"}], "]"}], ";"}]}], "Input"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"IQDE120k", "=", "%"}]], "Input"],

Cell[BoxData[
 RowBox[{"-", "136.3833557048152`"}]], "Output",
 CellChangeTimes->{3.4522338272862816`*^9, 3.452234353857666*^9, 
  3.4522367656080456`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"res", "=", 
   RowBox[{"NSolve", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"p1QFL", "[", "i", "]"}], "\[Equal]", "gLQFO135"}], ",", "i"}], 
    "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"(", 
    RowBox[{"i", "/.", "res"}], ")"}], "[", 
   RowBox[{"[", 
    RowBox[{
     RowBox[{"Position", "[", 
      RowBox[{
       RowBox[{
        RowBox[{
         RowBox[{"Im", "[", "#", "]"}], "&"}], "/@", 
        RowBox[{"(", 
         RowBox[{"i", "/.", "res"}], ")"}]}], ",", "0"}], "]"}], "[", 
     RowBox[{"[", 
      RowBox[{"1", ",", "1"}], "]"}], "]"}], "]"}], "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"IQFO135k", "=", "%"}]}], "Input"],

Cell[BoxData["122.58330746237253`"], "Output",
 CellChangeTimes->{3.4522338272862816`*^9, 3.452234353873291*^9, 
  3.4522367656080456`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"res", "=", 
   RowBox[{"NSolve", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"p1QFL", "[", "i", "]"}], "\[Equal]", "gLQDE150"}], ",", "i"}], 
    "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"(", 
    RowBox[{"i", "/.", "res"}], ")"}], "[", 
   RowBox[{"[", 
    RowBox[{
     RowBox[{"Position", "[", 
      RowBox[{
       RowBox[{
        RowBox[{
         RowBox[{"Im", "[", "#", "]"}], "&"}], "/@", 
        RowBox[{"(", 
         RowBox[{"i", "/.", "res"}], ")"}]}], ",", "0"}], "]"}], "[", 
     RowBox[{"[", 
      RowBox[{"1", ",", "1"}], "]"}], "]"}], "]"}], "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"IQDE150k", "=", "%"}]}], "Input"],

Cell[BoxData[
 RowBox[{"-", "108.3950159259495`"}]], "Output",
 CellChangeTimes->{3.452233827301906*^9, 3.452234353873291*^9, 
  3.45223676562367*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"res", "=", 
   RowBox[{"NSolve", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"p1QFL", "[", "i", "]"}], "\[Equal]", "gLQFO165"}], ",", "i"}], 
    "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"(", 
    RowBox[{"i", "/.", "res"}], ")"}], "[", 
   RowBox[{"[", 
    RowBox[{
     RowBox[{"Position", "[", 
      RowBox[{
       RowBox[{
        RowBox[{
         RowBox[{"Im", "[", "#", "]"}], "&"}], "/@", 
        RowBox[{"(", 
         RowBox[{"i", "/.", "res"}], ")"}]}], ",", "0"}], "]"}], "[", 
     RowBox[{"[", 
      RowBox[{"1", ",", "1"}], "]"}], "]"}], "]"}], "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"IQFO165k", "=", "%"}]}], "Input"],

Cell[BoxData["113.18678929895634`"], "Output",
 CellChangeTimes->{3.452233827301906*^9, 3.452234353888916*^9, 
  3.45223676562367*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"res", "=", 
   RowBox[{"NSolve", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"p1QFL", "[", "i", "]"}], "\[Equal]", "gLQDE180"}], ",", "i"}], 
    "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"(", 
    RowBox[{"i", "/.", "res"}], ")"}], "[", 
   RowBox[{"[", 
    RowBox[{
     RowBox[{"Position", "[", 
      RowBox[{
       RowBox[{
        RowBox[{
         RowBox[{"Im", "[", "#", "]"}], "&"}], "/@", 
        RowBox[{"(", 
         RowBox[{"i", "/.", "res"}], ")"}]}], ",", "0"}], "]"}], "[", 
     RowBox[{"[", 
      RowBox[{"1", ",", "1"}], "]"}], "]"}], "]"}], "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"IQDE180k", "=", "%"}]}], "Input"],

Cell[BoxData[
 RowBox[{"-", "112.8933090285222`"}]], "Output",
 CellChangeTimes->{3.452233827317531*^9, 3.452234353888916*^9, 
  3.452236765639295*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"res", "=", 
   RowBox[{"NSolve", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"p1QFL", "[", "i", "]"}], "\[Equal]", "gLQFO205"}], ",", "i"}], 
    "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"(", 
    RowBox[{"i", "/.", "res"}], ")"}], "[", 
   RowBox[{"[", 
    RowBox[{
     RowBox[{"Position", "[", 
      RowBox[{
       RowBox[{
        RowBox[{
         RowBox[{"Im", "[", "#", "]"}], "&"}], "/@", 
        RowBox[{"(", 
         RowBox[{"i", "/.", "res"}], ")"}]}], ",", "0"}], "]"}], "[", 
     RowBox[{"[", 
      RowBox[{"1", ",", "1"}], "]"}], "]"}], "]"}], "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"IQFO205k", "=", "%"}]}], "Input"],

Cell[BoxData["95.0777856323922`"], "Output",
 CellChangeTimes->{3.4522338273331556`*^9, 3.4522343539045405`*^9, 
  3.4522367656549196`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"res", "=", 
   RowBox[{"NSolve", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"p1QFL", "[", "i", "]"}], "\[Equal]", "gLQFO215"}], ",", "i"}], 
    "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"(", 
    RowBox[{"i", "/.", "res"}], ")"}], "[", 
   RowBox[{"[", 
    RowBox[{
     RowBox[{"Position", "[", 
      RowBox[{
       RowBox[{
        RowBox[{
         RowBox[{"Im", "[", "#", "]"}], "&"}], "/@", 
        RowBox[{"(", 
         RowBox[{"i", "/.", "res"}], ")"}]}], ",", "0"}], "]"}], "[", 
     RowBox[{"[", 
      RowBox[{"1", ",", "1"}], "]"}], "]"}], "]"}], "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"IQFO215k", "=", "%"}]}], "Input"],

Cell[BoxData["110.97760445157282`"], "Output",
 CellChangeTimes->{3.4522338273331556`*^9, 3.4522343539045405`*^9, 
  3.4522367656549196`*^9}]
}, Open  ]]
}, Open  ]]
}, Closed]],

Cell[CellGroupData[{

Cell["Type QFS (String QFO225.F)", "Subsubtitle"],

Cell[CellGroupData[{

Cell["QFS: MAD modelling", "Section"],

Cell["", "Text"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"LQFS", "=", "0.8"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"gLQFO225S", "=", 
   RowBox[{"k1QFO225S", " ", "LQFS", " ", "B\[Rho]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"gLQFO375", "=", 
   RowBox[{"k1QFO375", " ", "LQFS", " ", "B\[Rho]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"IQFO225SMAD", "=", 
   FractionBox[
    RowBox[{"k1QFO225S", " ", "momentum"}], "0.0130411"]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"IQFO375MAD", "=", 
   FractionBox[
    RowBox[{"k1QFO375", " ", "momentum"}], "0.0130411"]}], ";"}]}], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell["QFS (Q80):  7th order polynomials p1QFS", "Section"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"p1QFS", "[", "i_", "]"}], "=", 
    RowBox[{
     RowBox[{"0.3481475", " ", 
      SuperscriptBox["10", 
       RowBox[{"-", "1"}]], " ", "i"}], "+", 
     RowBox[{"0.9651494", " ", 
      SuperscriptBox["10", 
       RowBox[{"-", "8"}]], " ", 
      SuperscriptBox["i", "3"]}], "-", 
     RowBox[{"0.1239243", " ", 
      SuperscriptBox["10", 
       RowBox[{"-", "12"}]], " ", 
      SuperscriptBox["i", "5"]}], "+", 
     RowBox[{"0.1239355", " ", 
      SuperscriptBox["10", 
       RowBox[{"-", "18"}]], " ", 
      SuperscriptBox["i", "7"]}]}]}], ";"}], " "}], "\[IndentingNewLine]", 
 RowBox[{"res", "=", 
  RowBox[{"NSolve", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"p1QFS", "[", "i", "]"}], "\[Equal]", "gLQFO225S"}], ",", "i"}], 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"(", 
    RowBox[{"i", "/.", "res"}], ")"}], "[", 
   RowBox[{"[", 
    RowBox[{
     RowBox[{"Position", "[", 
      RowBox[{
       RowBox[{
        RowBox[{
         RowBox[{"Im", "[", "#", "]"}], "&"}], "/@", 
        RowBox[{"(", 
         RowBox[{"i", "/.", "res"}], ")"}]}], ",", "0"}], "]"}], "[", 
     RowBox[{"[", 
      RowBox[{"1", ",", "1"}], "]"}], "]"}], "]"}], "]"}], ";"}]}], "Input"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"i", "\[Rule]", 
     RowBox[{
      RowBox[{"-", "893.8113286794288`"}], "-", 
      RowBox[{"250.8813421722494`", " ", "\[ImaginaryI]"}]}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"i", "\[Rule]", 
     RowBox[{
      RowBox[{"-", "893.8113286794288`"}], "+", 
      RowBox[{"250.8813421722494`", " ", "\[ImaginaryI]"}]}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"i", "\[Rule]", 
     RowBox[{
      RowBox[{"-", "36.70110806188359`"}], "-", 
      RowBox[{"652.7488676632801`", " ", "\[ImaginaryI]"}]}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"i", "\[Rule]", 
     RowBox[{
      RowBox[{"-", "36.70110806188359`"}], "+", 
      RowBox[{"652.7488676632801`", " ", "\[ImaginaryI]"}]}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"i", "\[Rule]", "162.95126731587646`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"i", "\[Rule]", 
     RowBox[{"849.0368030833741`", "\[InvisibleSpace]", "-", 
      RowBox[{"213.19838381230826`", " ", "\[ImaginaryI]"}]}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"i", "\[Rule]", 
     RowBox[{"849.0368030833741`", "\[InvisibleSpace]", "+", 
      RowBox[{"213.19838381230826`", " ", "\[ImaginaryI]"}]}]}], "}"}]}], 
  "}"}]], "Output",
 CellChangeTimes->{3.4522338273487806`*^9, 3.452234353920165*^9, 
  3.4522367656705446`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"IQFO225Sk", "=", "%"}]], "Input"],

Cell[BoxData["162.95126731587646`"], "Output",
 CellChangeTimes->{3.4522338273644056`*^9, 3.45223435393579*^9, 
  3.4522367656705446`*^9}]
}, Open  ]],

Cell[BoxData[{
 RowBox[{
  RowBox[{"res", "=", 
   RowBox[{"NSolve", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"p1QFS", "[", "i", "]"}], "\[Equal]", "gLQFO375"}], ",", "i"}], 
    "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"(", 
    RowBox[{"i", "/.", "res"}], ")"}], "[", 
   RowBox[{"[", 
    RowBox[{
     RowBox[{"Position", "[", 
      RowBox[{
       RowBox[{
        RowBox[{
         RowBox[{"Im", "[", "#", "]"}], "&"}], "/@", 
        RowBox[{"(", 
         RowBox[{"i", "/.", "res"}], ")"}]}], ",", "0"}], "]"}], "[", 
     RowBox[{"[", 
      RowBox[{"1", ",", "1"}], "]"}], "]"}], "]"}], "]"}], ";"}]}], "Input"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"IQFO375k", "=", "%"}]], "Input"],

Cell[BoxData["201.43729987133216`"], "Output",
 CellChangeTimes->{3.4522338273644056`*^9, 3.45223435393579*^9, 
  3.4522367656861696`*^9}]
}, Open  ]]
}, Closed]]
}, Closed]],

Cell[CellGroupData[{

Cell["Type QD (String QDE240.F)", "Subsubtitle"],

Cell[CellGroupData[{

Cell["QD: MAD modelling", "Section"],

Cell["", "Text"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"LQD", "=", "0.82"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"gLQDE210", "=", 
   RowBox[{"k1QDE210", " ", "LQD", " ", "B\[Rho]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"IQDE210MAD", "=", 
   FractionBox[
    RowBox[{"k1QDE210", " ", "momentum"}], "0.0130155"]}], ";"}]}], "Input"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"LQD", "=", "0.82"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"gLQDE240S", "=", 
   RowBox[{"k1QDE240S", " ", "LQD", " ", "B\[Rho]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"IQDE240SMAD", "=", 
   FractionBox[
    RowBox[{"k1QDE240S", " ", "momentum"}], "0.0130155"]}], ";"}]}], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell["QD (Q82):  5th order polynomials p1QD", "Section"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"p1QD", "[", "i_", "]"}], "=", 
    RowBox[{
     RowBox[{"0.3555191", " ", 
      SuperscriptBox["10", 
       RowBox[{"-", "1"}]], " ", "i"}], "+", 
     RowBox[{"0.6142503", " ", 
      SuperscriptBox["10", 
       RowBox[{"-", "8"}]], " ", 
      SuperscriptBox["i", "3"]}], "-", 
     RowBox[{"0.6931895", " ", 
      SuperscriptBox["10", 
       RowBox[{"-", "13"}]], " ", 
      SuperscriptBox["i", "5"]}]}]}], ";"}], " "}]], "Input"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"res", "=", 
   RowBox[{"NSolve", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"p1QD", "[", "i", "]"}], "\[Equal]", "gLQDE210"}], ",", "i"}], 
    "]"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{"i", " ", "/.", " ", 
   RowBox[{"res", "[", 
    RowBox[{"[", "4", "]"}], "]"}]}], ";"}], "\n", 
 RowBox[{"IQDE210k", "=", "%"}]}], "Input"],

Cell[BoxData["159.8711902847877`"], "Output",
 CellChangeTimes->{3.45223382738003*^9, 3.452234353951415*^9, 
  3.452236765701794*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"res", "=", 
   RowBox[{"NSolve", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"p1QD", "[", "i", "]"}], "\[Equal]", "gLQDE240S"}], ",", "i"}], 
    "]"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{"i", " ", "/.", " ", 
   RowBox[{"res", "[", 
    RowBox[{"[", "4", "]"}], "]"}]}], ";"}], "\n", 
 RowBox[{"IQDE240Sk", "=", "%"}]}], "Input"],

Cell[BoxData["159.87119034520398`"], "Output",
 CellChangeTimes->{3.4522338273956547`*^9, 3.45223435396704*^9, 
  3.4522367657174187`*^9}]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Line fittings for p1QD  ", "Section"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"xpoints", "=", 
   RowBox[{"{", 
    RowBox[{
    "50", ",", "100", ",", "150", ",", "200", ",", "250", ",", "300", ",", 
     "350", ",", "400"}], "}"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"points", "=", 
   RowBox[{"Transpose", "[", 
    RowBox[{"{", 
     RowBox[{"xpoints", ",", 
      RowBox[{"p1QD", "[", "xpoints", "]"}]}], "}"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"p1aQD", "[", "x_", "]"}], "=", 
  RowBox[{"Fit", "[", 
   RowBox[{"points", ",", 
    RowBox[{"{", 
     RowBox[{"1", ",", "x"}], "}"}], ",", "x"}], 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"p1bQD", "[", "x_", "]"}], "=", 
  RowBox[{"Fit", "[", 
   RowBox[{"points", ",", 
    RowBox[{"{", "x", "}"}], ",", "x"}], "]"}]}], "\[IndentingNewLine]", 
 RowBox[{"NSolve", "[", 
  RowBox[{
   RowBox[{
    RowBox[{"p1aQD", "[", "i", "]"}], "\[Equal]", "gL"}], ",", "i"}], 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{"NSolve", "[", 
  RowBox[{
   RowBox[{
    RowBox[{"p1bQD", "[", "i", "]"}], "\[Equal]", "gL"}], ",", "i"}], 
  "]"}]}], "Input"],

Cell[BoxData[
 RowBox[{"0.10742439312723215`", "\[InvisibleSpace]", "+", 
  RowBox[{"0.03488384778401786`", " ", "x"}]}]], "Output",
 CellChangeTimes->{3.452233827583153*^9, 3.45223435396704*^9, 
  3.4522367657174187`*^9}],

Cell[BoxData[
 RowBox[{"0.0352629927009375`", " ", "x"}]], "Output",
 CellChangeTimes->{3.452233827583153*^9, 3.45223435396704*^9, 
  3.4522367657174187`*^9}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"{", 
   RowBox[{"i", "\[Rule]", 
    RowBox[{"28.666562421423965`", " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", "0.10742439312723215`"}], "+", 
       RowBox[{"1.`", " ", "gL"}]}], ")"}]}]}], "}"}], "}"}]], "Output",
 CellChangeTimes->{3.452233827583153*^9, 3.45223435396704*^9, 
  3.4522367657174187`*^9}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"{", 
   RowBox[{"i", "\[Rule]", 
    RowBox[{"28.358341802719824`", " ", "gL"}]}], "}"}], "}"}]], "Output",
 CellChangeTimes->{3.452233827583153*^9, 3.45223435396704*^9, 
  3.4522367657174187`*^9}]
}, Open  ]]
}, Open  ]]
}, Closed]],

Cell[CellGroupData[{

Cell["All quads strengths from current ", "Subsubtitle"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"k1QFO105I", "=", 
   FractionBox[
    RowBox[{"Mean", "[", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"p1QF101", "[", "IQFO105", "]"}], ",", 
       RowBox[{"p2QF101", "[", "IQFO105", "]"}]}], "}"}], "]"}], 
    RowBox[{"B\[Rho]", " ", "LQF101"}]]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"k1QDE120I", "=", 
   FractionBox[
    RowBox[{"p1QFL", "[", "IQDE120", "]"}], 
    RowBox[{"B\[Rho]", " ", "LQFL"}]]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"k1QDE120I", "=", 
   FractionBox[
    RowBox[{"p1QFL", "[", "IQDE120", "]"}], 
    RowBox[{"B\[Rho]", " ", "LQFL"}]]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"k1QFO135I", "=", 
   FractionBox[
    RowBox[{"p1QFL", "[", "IQFO135", "]"}], 
    RowBox[{"B\[Rho]", " ", "LQFL"}]]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"k1QDE150I", "=", 
   FractionBox[
    RowBox[{"p1QFL", "[", "IQDE150", "]"}], 
    RowBox[{"B\[Rho]", " ", "LQFL"}]]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"k1QFO165I", "=", 
   FractionBox[
    RowBox[{"p1QFL", "[", "IQFO165", "]"}], 
    RowBox[{"B\[Rho]", " ", "LQFL"}]]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"k1QDE180I", "=", 
   FractionBox[
    RowBox[{"p1QFL", "[", "IQDE180", "]"}], 
    RowBox[{"B\[Rho]", " ", "LQFL"}]]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"k1QFO205I", "=", 
   FractionBox[
    RowBox[{"p1QFL", "[", "IQFO205", "]"}], 
    RowBox[{"B\[Rho]", " ", "LQFL"}]]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"k1QDE210I", "=", 
   FractionBox[
    RowBox[{"p1QD", "[", "IQDE210", "]"}], 
    RowBox[{"B\[Rho]", " ", "LQD"}]]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"k1QDE240SI", "=", 
   FractionBox[
    RowBox[{"p1QD", "[", "IQDE240S", "]"}], 
    RowBox[{"B\[Rho]", " ", "LQD"}]]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"k1QFO215I", "=", 
    FractionBox[
     RowBox[{"p1QFL", "[", "IQFO215", "]"}], 
     RowBox[{"B\[Rho]", " ", "LQFL"}]]}], ";"}], "  ", 
  RowBox[{"(*", " ", 
   RowBox[{"1.2", "m", " ", "long"}], " ", "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"k1QFO225SI", "=", 
   FractionBox[
    RowBox[{"p1QFS", "[", "IQFO225S", "]"}], 
    RowBox[{"B\[Rho]", " ", "LQFS"}]]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"k1QFO375I", "=", 
   FractionBox[
    RowBox[{"p1QFS", "[", "IQFO375", "]"}], 
    RowBox[{"B\[Rho]", " ", "LQFS"}]]}], 
  ";"}], "\[IndentingNewLine]"}], "Input"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"k1QFO105IMAD", "=", 
   FractionBox[
    RowBox[{"0.0097608", "*", "IQFO105"}], "momentum"]}], ";"}], "\n", 
 RowBox[{
  RowBox[{"k1QDE120IMAD", "=", 
   FractionBox[
    RowBox[{"0.0128163", "*", "IQDE120"}], "momentum"]}], ";"}], "\n", 
 RowBox[{
  RowBox[{"k1QFO135IMAD", "=", 
   FractionBox[
    RowBox[{"0.0128163", "*", "IQFO135"}], "momentum"]}], ";"}], "\n", 
 RowBox[{
  RowBox[{"k1QDE150IMAD", "=", 
   FractionBox[
    RowBox[{"0.0128163", "*", "IQDE150"}], "momentum"]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"k1QFO165IMAD", "=", 
   FractionBox[
    RowBox[{"0.0128163", "*", "IQFO165"}], "momentum"]}], ";"}], "\n", 
 RowBox[{
  RowBox[{"k1QDE180IMAD", "=", 
   FractionBox[
    RowBox[{"0.0128163", "*", "IQDE180"}], "momentum"]}], ";"}], "\n", 
 RowBox[{
  RowBox[{"k1QFO205IMAD", "=", 
   FractionBox[
    RowBox[{"0.0128163", "*", "IQFO205"}], "momentum"]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"k1QDE210IMAD", "=", 
   FractionBox[
    RowBox[{"0.0130155", "*", "IQDE210"}], "momentum"]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"k1QDE240SIMAD", "=", 
   FractionBox[
    RowBox[{"0.0130155", "*", "IQDE240S"}], "momentum"]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"k1QFO215IMAD", "=", 
  FractionBox[
   RowBox[{"0.0128163", "*", "IQFO215"}], 
   "momentum"]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"k1QFO225SIMAD", "=", 
   FractionBox[
    RowBox[{"0.0130411", "*", "IQFO225S"}], "momentum"]}], ";"}], "\n", 
 RowBox[{
  RowBox[{"k1QFO375IMAD", "=", 
   FractionBox[
    RowBox[{"0.0130411", "*", "IQFO375"}], "momentum"]}], ";"}]}], "Input"],

Cell[BoxData["0.07111611359663464`"], "Output",
 CellChangeTimes->{3.452233827630027*^9, 3.4522343539826646`*^9, 
  3.4522367657330437`*^9, 3.452237039698287*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"strengthMAD", "=", 
  RowBox[{"{", 
   RowBox[{
   "k1QFO105IMAD", ",", "k1QDE120IMAD", ",", "k1QFO135IMAD", ",", 
    "k1QDE150IMAD", ",", "k1QFO165IMAD", ",", "k1QDE180IMAD", ",", 
    "k1QFO205IMAD", ",", "k1QDE210IMAD", ",", "k1QFO215IMAD", ",", 
    RowBox[{"k1QDE240SIMAD", "*", "1.001966885636"}], ",", "k1QFO225SIMAD", 
    ",", "k1QDE240SIMAD", ",", "k1QFO375IMAD"}], "}"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"0.1992204760932447`", ",", 
   RowBox[{"-", "0.08739650008598114`"}], ",", "0.07855322217150025`", ",", 
   RowBox[{"-", "0.06946115213058732`"}], ",", "0.0725317923846107`", ",", 
   RowBox[{"-", "0.07234372582511246`"}], ",", "0.06092727120002141`", ",", 
   "0.10404017385758271`", ",", "0.07111611359663464`", ",", 
   "0.10424480902050488`", ",", "0.10625318860965383`", ",", 
   "0.10404017389690012`", ",", "0.1313481985676015`"}], "}"}]], "Output",
 CellChangeTimes->{3.452233827630027*^9, 3.452234353998289*^9, 
  3.4522367657330437`*^9, 3.452237039713912*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{"strengthnew", "=", 
   RowBox[{"{", 
    RowBox[{
    "k1QFO105I", ",", "k1QDE120I", ",", "k1QFO135I", ",", "k1QDE150I", ",", 
     "k1QFO165I", ",", "k1QDE180I", ",", "k1QFO205I", ",", "k1QDE210I", ",", 
     "k1QFO215I", ",", 
     RowBox[{"k1QDE240SI", "*", "1.001966885636"}], ",", "k1QFO225SI", ",", 
     "k1QDE240SI", ",", "k1QFO375I"}], "}"}]}], ";"}]], "Input"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Inputcurrent", "=", 
  RowBox[{"{", 
   RowBox[{
   "IQFO105", ",", "IQDE120", ",", "IQFO135", ",", "IQDE150", ",", "IQFO165", 
    ",", "IQDE180", ",", "IQFO205", ",", "IQDE210", ",", "IQFO215", ",", 
    "IQDE220S", ",", "IQFO225S", ",", "IQDE240S", ",", "IQFO375"}], 
   "}"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"408.2052210745936`", ",", 
   RowBox[{"-", "136.3833557048152`"}], ",", "122.58330746237253`", ",", 
   RowBox[{"-", "108.3950159259495`"}], ",", "113.18678929895634`", ",", 
   RowBox[{"-", "112.8933090285222`"}], ",", "95.0777856323922`", ",", 
   "159.8711902847877`", ",", "110.97760445157282`", ",", "IQDE220S", ",", 
   "162.95126731587646`", ",", "159.87119034520398`", ",", 
   "201.43729987133216`"}], "}"}]], "Output",
 CellChangeTimes->{3.452233827645652*^9, 3.452234353998289*^9, 
  3.4522367657486687`*^9, 3.452237039713912*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Namequad", "=", 
  RowBox[{"{", 
   RowBox[{
   "\"\<QFO105\>\"", ",", "\"\<QDE120\>\"", ",", "\"\<QFO135\>\"", ",", 
    "\"\<IQDE150\>\"", ",", "\"\<IQFO165\>\"", ",", "\"\<IQDE180\>\"", ",", 
    "\"\<IQFO205\>\"", ",", "\"\<IQDE210\>\"", ",", "\"\<IQFO215\>\"", ",", 
    "\"\<IQDE220S\>\"", ",", "\"\<IQFO225S\>\"", ",", "\"\<IQDE240S\>\"", 
    ",", "\"\<IQFO375\>\""}], "}"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"\<\"QFO105\"\>", ",", "\<\"QDE120\"\>", ",", "\<\"QFO135\"\>", 
   ",", "\<\"IQDE150\"\>", ",", "\<\"IQFO165\"\>", ",", "\<\"IQDE180\"\>", 
   ",", "\<\"IQFO205\"\>", ",", "\<\"IQDE210\"\>", ",", "\<\"IQFO215\"\>", 
   ",", "\<\"IQDE220S\"\>", ",", "\<\"IQFO225S\"\>", ",", "\<\"IQDE240S\"\>", 
   ",", "\<\"IQFO375\"\>"}], "}"}]], "Output",
 CellChangeTimes->{3.452233827661277*^9, 3.452234354013914*^9, 
  3.4522367657642937`*^9, 3.452237039713912*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"MatrixForm", "[", 
  RowBox[{"{", 
   RowBox[{
   "Namequad", ",", "Inputcurrent", ",", "strengthMAD", ",", "strengthnew"}], 
   "}"}], "]"}]], "Input"],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {"\<\"QFO105\"\>", "\<\"QDE120\"\>", "\<\"QFO135\"\>", \
"\<\"IQDE150\"\>", "\<\"IQFO165\"\>", "\<\"IQDE180\"\>", "\<\"IQFO205\"\>", "\
\<\"IQDE210\"\>", "\<\"IQFO215\"\>", "\<\"IQDE220S\"\>", "\<\"IQFO225S\"\>", \
"\<\"IQDE240S\"\>", "\<\"IQFO375\"\>"},
     {"408.2052210745936`", 
      RowBox[{"-", "136.3833557048152`"}], "122.58330746237253`", 
      RowBox[{"-", "108.3950159259495`"}], "113.18678929895634`", 
      RowBox[{"-", "112.8933090285222`"}], "95.0777856323922`", 
      "159.8711902847877`", "110.97760445157282`", "IQDE220S", 
      "162.95126731587646`", "159.87119034520398`", "201.43729987133216`"},
     {"0.1992204760932447`", 
      RowBox[{"-", "0.08739650008598114`"}], "0.07855322217150025`", 
      RowBox[{"-", "0.06946115213058732`"}], "0.0725317923846107`", 
      RowBox[{"-", "0.07234372582511246`"}], "0.06092727120002141`", 
      "0.10404017385758271`", "0.07111611359663464`", "0.10424480902050488`", 
      "0.10625318860965383`", "0.10404017389690012`", "0.1313481985676015`"},
     {"0.20394394899207985`", 
      RowBox[{"-", "0.08760635913`"}], "0.07869850885999999`", 
      RowBox[{"-", "0.06954970924999998`"}], "0.07263829230999999`", 
      RowBox[{"-", "0.07244908999999998`"}], "0.06097295`", "0.1042264261`", 
      "0.07121418127999998`", "0.10443142759999999`", "0.10682127190000001`", 
      "0.10422642613953452`", "0.1321434752`"}
    },
    GridBoxAlignment->{
     "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{3.452233827723776*^9, 3.452234354013914*^9, 
  3.4522367657642937`*^9, 3.4522370397295365`*^9}]
}, Open  ]]
}, Closed]],

Cell[CellGroupData[{

Cell["All quads currents from strengths", "Subsubtitle"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Inputstrength", "=", 
   RowBox[{"{", 
    RowBox[{
    "k1QFO105", ",", "k1QDE120", ",", "k1QFO135", ",", "k1QDE150", ",", 
     "k1QFO165", ",", "k1QDE180", ",", "k1QFO205", ",", "k1QDE210", ",", 
     "k1QFO215", ",", "k1QDE220S", ",", "k1QFO225S", ",", "k1QDE240S", ",", 
     "k1QFO375"}], "}"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"CurrentMAD", "=", 
   RowBox[{"{", 
    RowBox[{
    "IQFO105MAD", ",", "IQDE120MAD", ",", "IQFO135MAD", ",", "IQDE150MAD", 
     ",", "IQFO165MAD", ",", "IQDE180MAD", ",", "IQFO205MAD", ",", 
     "IQDE210MAD", ",", " ", "IQFO215MAD", ",", 
     RowBox[{"IQDE240SMAD", "*", "1.001966885636"}], ",", "IQFO225SMAD", ",", 
     "IQDE240SMAD", ",", "IQFO375MAD"}], "}"}]}], ";"}]], "Input"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Currentnew", "=", 
  RowBox[{"{", 
   RowBox[{
   "IQFO105k", ",", "IQDE120k", ",", "IQFO135k", ",", "IQDE150k", ",", 
    "IQFO165k", ",", "IQDE180k", ",", "IQFO205k", ",", "IQDE210k", ",", 
    "IQFO215k", ",", 
    RowBox[{"IQDE240Sk", "*", "1.001966885636"}], ",", "IQFO225Sk", ",", 
    "IQDE240Sk", ",", "IQFO375k"}], "}"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"408.2052210745936`", ",", 
   RowBox[{"-", "136.3833557048152`"}], ",", "122.58330746237253`", ",", 
   RowBox[{"-", "108.3950159259495`"}], ",", "113.18678929895634`", ",", 
   RowBox[{"-", "112.8933090285222`"}], ",", "95.0777856323922`", ",", 
   "159.8711902847877`", ",", "110.97760445157282`", ",", 
   "160.18563869310418`", ",", "162.95126731587646`", ",", 
   "159.87119034520398`", ",", "201.43729987133216`"}], "}"}]], "Output",
 CellChangeTimes->{3.452233827739401*^9, 3.452234354029539*^9, 
  3.452236765779918*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Namequad", "=", 
  RowBox[{"{", 
   RowBox[{
   "\"\<QFO105\>\"", ",", "\"\<QDE120\>\"", ",", "\"\<QFO135\>\"", ",", 
    "\"\<IQDE150\>\"", ",", "\"\<IQFO165\>\"", ",", "\"\<IQDE180\>\"", ",", 
    "\"\<IQFO205\>\"", ",", "\"\<IQDE210\>\"", ",", "\"\<IQFO215\>\"", ",", 
    "\"\<IQDE220S\>\"", ",", "\"\<IQFO225S\>\"", ",", "\"\<IQDE240S\>\"", 
    ",", "\"\<IQFO375\>\""}], "}"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"\<\"QFO105\"\>", ",", "\<\"QDE120\"\>", ",", "\<\"QFO135\"\>", 
   ",", "\<\"IQDE150\"\>", ",", "\<\"IQFO165\"\>", ",", "\<\"IQDE180\"\>", 
   ",", "\<\"IQFO205\"\>", ",", "\<\"IQDE210\"\>", ",", "\<\"IQFO215\"\>", 
   ",", "\<\"IQDE220S\"\>", ",", "\<\"IQFO225S\"\>", ",", "\<\"IQDE240S\"\>", 
   ",", "\<\"IQFO375\"\>"}], "}"}]], "Output",
 CellChangeTimes->{3.4522338277550254`*^9, 3.452234354045164*^9, 
  3.4522367657955427`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"MatrixForm", "[", 
  RowBox[{"{", 
   RowBox[{
   "Namequad", ",", "Inputstrength", ",", "CurrentMAD", ",", "Currentnew"}], 
   "}"}], "]"}]], "Input"],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {"\<\"QFO105\"\>", "\<\"QDE120\"\>", "\<\"QFO135\"\>", \
"\<\"IQDE150\"\>", "\<\"IQFO165\"\>", "\<\"IQDE180\"\>", "\<\"IQFO205\"\>", "\
\<\"IQDE210\"\>", "\<\"IQFO215\"\>", "\<\"IQDE220S\"\>", "\<\"IQFO225S\"\>", \
"\<\"IQDE240S\"\>", "\<\"IQFO375\"\>"},
     {"0.20394437`", 
      RowBox[{"-", "0.08760635913`"}], "0.07869850886`", 
      RowBox[{"-", "0.06954970925`"}], "0.07263829231`", 
      RowBox[{"-", "0.07244909`"}], "0.06097295`", "0.1042264261`", 
      "0.07121418128`", "k1QDE220S", "0.1068212719`", "0.10422642613953453`", 
      "0.1321434752`"},
     {"417.8845381526105`", 
      RowBox[{"-", "136.7108434259498`"}], "122.81002919719421`", 
      RowBox[{"-", "108.53321044295156`"}], "113.35298379407476`", 
      RowBox[{"-", "113.05773117046262`"}], "95.14906798373947`", 
      "160.15739095693598`", "111.13064032521088`", "160.47240228957784`", 
      "163.82248721350194`", "160.1573910176859`", "202.6569464232312`"},
     {"408.2052210745936`", 
      RowBox[{"-", "136.3833557048152`"}], "122.58330746237253`", 
      RowBox[{"-", "108.3950159259495`"}], "113.18678929895634`", 
      RowBox[{"-", "112.8933090285222`"}], "95.0777856323922`", 
      "159.8711902847877`", "110.97760445157282`", "160.18563869310418`", 
      "162.95126731587646`", "159.87119034520398`", "201.43729987133216`"}
    },
    GridBoxAlignment->{
     "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{3.4522338277550254`*^9, 3.452234354045164*^9, 
  3.4522367657955427`*^9}]
}, Open  ]]
}, Closed]]
},
WindowSize->{800, 808},
WindowMargins->{{45, Automatic}, {Automatic, 4}},
PrintingCopies->1,
PrintingPageRange->{1, 2},
PrivateNotebookOptions->{"VersionedStylesheet"->{"Default.nb"[8.] -> True}},
FrontEndVersion->"9.0 for Microsoft Windows (32-bit) (January 25, 2013)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[1463, 33, 4265, 106, 312, "Input"],
Cell[5731, 141, 185, 5, 31, "Input"],
Cell[CellGroupData[{
Cell[5941, 150, 77, 1, 31, "Input"],
Cell[6021, 153, 645, 12, 56, "Message"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6703, 170, 125, 2, 31, "Input"],
Cell[6831, 174, 124, 2, 30, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6992, 181, 90, 2, 31, "Input"],
Cell[7085, 185, 127, 2, 30, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7249, 192, 144, 3, 26, "Subsubtitle"],
Cell[CellGroupData[{
Cell[7418, 199, 34, 0, 71, "Section"],
Cell[7455, 201, 2569, 68, 552, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[10061, 274, 34, 0, 71, "Section"],
Cell[10098, 276, 2784, 75, 432, "Input"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[12931, 357, 42, 0, 28, "Subsubtitle"],
Cell[CellGroupData[{
Cell[12998, 361, 39, 0, 71, "Section"],
Cell[13040, 363, 57, 0, 29, "Text"],
Cell[13100, 365, 354, 10, 85, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[13491, 380, 75, 0, 71, "Section"],
Cell[CellGroupData[{
Cell[13591, 384, 1271, 41, 72, "Input"],
Cell[14865, 427, 136, 2, 30, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[15038, 434, 56, 1, 31, "Input"],
Cell[15097, 437, 136, 2, 30, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[15270, 444, 332, 10, 31, "Input"],
Cell[15605, 456, 138, 2, 30, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[15780, 463, 56, 1, 31, "Input"],
Cell[15839, 466, 140, 2, 30, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[16016, 473, 150, 4, 31, "Input"],
Cell[16169, 479, 140, 2, 30, "Output"]
}, Open  ]]
}, Open  ]]
}, Closed]],
Cell[CellGroupData[{
Cell[16370, 488, 95, 2, 28, "Subsubtitle"],
Cell[CellGroupData[{
Cell[16490, 494, 37, 0, 70, "Section"],
Cell[16530, 496, 16, 0, 70, "Text"],
Cell[16549, 498, 2136, 67, 70, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[18722, 570, 61, 0, 70, "Section"],
Cell[18786, 572, 1295, 42, 70, "Input"],
Cell[CellGroupData[{
Cell[20106, 618, 56, 1, 70, "Input"],
Cell[20165, 621, 155, 3, 70, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[20357, 629, 719, 24, 70, "Input"],
Cell[21079, 655, 139, 2, 70, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[21255, 662, 719, 24, 70, "Input"],
Cell[21977, 688, 150, 3, 70, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[22164, 696, 719, 24, 70, "Input"],
Cell[22886, 722, 134, 2, 70, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[23057, 729, 719, 24, 70, "Input"],
Cell[23779, 755, 151, 3, 70, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[23967, 763, 719, 24, 70, "Input"],
Cell[24689, 789, 139, 2, 70, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[24865, 796, 719, 24, 70, "Input"],
Cell[25587, 822, 141, 2, 70, "Output"]
}, Open  ]]
}, Open  ]]
}, Closed]],
Cell[CellGroupData[{
Cell[25789, 831, 49, 0, 28, "Subsubtitle"],
Cell[CellGroupData[{
Cell[25863, 835, 37, 0, 71, "Section"],
Cell[25903, 837, 16, 0, 29, "Text"],
Cell[25922, 839, 629, 19, 139, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[26588, 863, 58, 0, 71, "Section"],
Cell[CellGroupData[{
Cell[26671, 867, 1274, 41, 70, "Input"],
Cell[27948, 910, 1348, 35, 70, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[29333, 950, 57, 1, 70, "Input"],
Cell[29393, 953, 138, 2, 70, "Output"]
}, Open  ]],
Cell[29546, 958, 659, 22, 70, "Input"],
Cell[CellGroupData[{
Cell[30230, 984, 56, 1, 70, "Input"],
Cell[30289, 987, 138, 2, 70, "Output"]
}, Open  ]]
}, Closed]]
}, Closed]],
Cell[CellGroupData[{
Cell[30488, 996, 48, 0, 28, "Subsubtitle"],
Cell[CellGroupData[{
Cell[30561, 1000, 36, 0, 71, "Section"],
Cell[30600, 1002, 16, 0, 29, "Text"],
Cell[30619, 1004, 347, 10, 85, "Input"],
Cell[30969, 1016, 351, 10, 85, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[31357, 1031, 56, 0, 71, "Section"],
Cell[31416, 1033, 500, 16, 31, "Input"],
Cell[CellGroupData[{
Cell[31941, 1053, 368, 12, 72, "Input"],
Cell[32312, 1067, 133, 2, 30, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[32482, 1074, 370, 12, 72, "Input"],
Cell[32855, 1088, 138, 2, 30, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[33042, 1096, 43, 0, 71, "Section"],
Cell[CellGroupData[{
Cell[33110, 1100, 1109, 35, 132, "Input"],
Cell[34222, 1137, 222, 4, 30, "Output"],
Cell[34447, 1143, 158, 3, 30, "Output"],
Cell[34608, 1148, 362, 10, 30, "Output"],
Cell[34973, 1160, 237, 6, 30, "Output"]
}, Open  ]]
}, Open  ]]
}, Closed]],
Cell[CellGroupData[{
Cell[35271, 1173, 56, 0, 28, "Subsubtitle"],
Cell[35330, 1175, 2498, 73, 513, "Input"],
Cell[CellGroupData[{
Cell[37853, 1252, 1663, 52, 419, "Input"],
Cell[39519, 1306, 162, 2, 30, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[39718, 1313, 419, 8, 72, "Input"],
Cell[40140, 1323, 609, 10, 50, "Output"]
}, Open  ]],
Cell[40764, 1336, 405, 9, 52, "Input"],
Cell[CellGroupData[{
Cell[41194, 1349, 316, 7, 52, "Input"],
Cell[41513, 1358, 581, 10, 50, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[42131, 1373, 419, 8, 52, "Input"],
Cell[42553, 1383, 494, 8, 50, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[43084, 1396, 176, 5, 31, "Input"],
Cell[43263, 1403, 2058, 39, 86, "Output"]
}, Open  ]]
}, Closed]],
Cell[CellGroupData[{
Cell[45370, 1448, 56, 0, 28, "Subsubtitle"],
Cell[45429, 1450, 355, 8, 52, "Input"],
Cell[45787, 1460, 422, 9, 72, "Input"],
Cell[CellGroupData[{
Cell[46234, 1473, 366, 8, 52, "Input"],
Cell[46603, 1483, 568, 10, 50, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[47208, 1498, 419, 8, 52, "Input"],
Cell[47630, 1508, 474, 8, 50, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[48141, 1521, 175, 5, 31, "Input"],
Cell[48319, 1528, 1972, 39, 86, "Output"]
}, Open  ]]
}, Closed]]
}
]
*)

(* End of internal cache information *)

(* NotebookSignature sv0b#9LWVjv7ADKvivDZ49PR *)
