/******************************************************************
 * MAD-X PS-to-TT10 to calculate initial conditions for TT2
 ** for LHC Q20 optics beams
 *
 ** 07/08/2019
 ** F.Velotti, M. Fraser
 ******************************************************************/
option, RBARC=FALSE;

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Load needed repos
***************************************/

system, "ln -fns ./../../../../../PS/2019/scenarios/LHC_PROTON/3_extraction ps_lhc_repo";
system, "ln -fns ./../../../../../PS/2019 ps_repo";

/******************************************************************
 * Call lattice files
 ******************************************************************/

option, -warn;
call, file="ps_repo/PS_MU.seq";
call, file="ps_repo/PS_SS.seq";
call, file="ps_repo/PS.str";
call, file="ps_lhc_repo/PS_EXT_LHC.str";
call, file="fringe_field_fix.madx";
call, file="./extraction_elements.seq";
option, warn;

/*******************************************************************************
 * Beam
 *******************************************************************************/
 Beam, particle=PROTON,pc=26,exn=3.5e-6,eyn=3.5E-6;
set,  format="10.5f";

/******************************************************************
 * Twiss with bump on
 ******************************************************************/

use, sequence=PS;
twiss;

/**********************************************
* Make extraction sequence to get to where there
* is the handover with TT2
**********************************************/

! Nominal kick strength for KFA71 for nTOF => assumed the same for AD and LHC
kfa71_nom_kick = -1.85e-03;

KFA7179: HKICKER, L:=PE.KFA71->L, KICK:= kfa71_k;
KFA45 :  HKICKER, L:=PI.KFA45->L, KICK:= kfa45_k;
KFA28 :  HKICKER, L:=PI.KFA28->L, KICK:= kfa28_k;

SEQEDIT, sequence=PS;
FLATTEN;
REPLACE, ELEMENT=PI.KFA45, BY=KFA45;
REPLACE, ELEMENT=PI.KFA28, BY=KFA28;
REPLACE, ELEMENT=PE.KFA71, BY=KFA7179;
FLATTEN;
ENDEDIT;

use, sequence = PS;

! Cycle sequence and changing starting point

SEQEDIT, sequence=PS;
CYCLE, START=PR.BPM17;
FLATTEN;
ENDEDIT;

use, sequence=PS;
twiss;

start_psej = table(twiss, PR.BPM23, s);
end_psej = table(twiss, PE.BTV16, s);

len_psej = end_psej - start_psej;

value, len_psej;

/*******************************************
* Install septum and consider fringe field
*******************************************/

len_ft16 = 7.701173928;


SEPTUM16E: MARKER;
FT16: sequence, refer=entry, l=len_ft16;
MTV001        ,at=0.0;
SEPTUM16      ,at=0.0;
SEPTUM16E       ,at=2.700072902;
D16STRAY      ,at=2.700072902;
F16SHIM       ,at=4.904893572;
pointR        ,at=7.701173928;
ENDSEQUENCE;

! POINTR is the handover point between PS and TT2 => initial conditions

/***********************************
* PS_EJ sequence definition
***********************************/

EXTRACT, sequence=PS, FROM=PR.BPM23, TO=PE.BTV16, newname=PS_EJ;


PS_EXTRACT: sequence, refer=entry, l=len_psej + len_ft16 ;
PS_EJ,       at=0.0;
FT16,        at=len_psej;
ENDSEQUENCE;

kfa71_k = 0.0;
kfa45_k = 0;
kfa28_k = 0;

USE, sequence=PS;
SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,KMIN,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
savebeta, label=bumped, place = PR.BPM23;
twiss, file = "PS_EXT_LHCQ20_bump.tfs";

use, sequence = PS_EXTRACT;
kfa71_k = kfa71_nom_kick;
kfa45_k = 0;
kfa28_k = 0;

twiss, beta0 = bumped;

x_stray_field = table(twiss, SEPTUM16E, X);
px_stray_field = table(twiss, SEPTUM16E, PX);

exec, place_stray_field(x_stray_field, px_stray_field);

savebeta,label=initial_cond, place = POINTR;
twiss, beta0 = bumped, file = "twiss_PS_to_TT10_lhcQ20_kicked.tfs";

/***********************************************
* Save initial parameters to file for TL usage
***********************************************/
assign, echo="tt2_tt10_lhcQ20_from_stitched_kickers.inp";

betx0 = initial_cond->betx;
bety0 =  initial_cond->bety;

alfx0 = initial_cond->alfx;
alfy0 = initial_cond->alfy;

dx0 = initial_cond->dx;
dy0 = initial_cond->dy;

dpx0 = initial_cond->dpx;
dpy0 = initial_cond->dpy;

print, text="/*********************************************************************************";
print, text='Initial conditions from MADX stitched model of PS extraction to TT10 LHC Q20 beam';
print, text="*********************************************************************************/";

print, text = '';
value,betx0;
value,bety0;
      
value,alfx0;
value,alfy0;
      
value,dx0 ;
value,dy0 ;
      
value,dpx0;
value,dpy0;

assign, echo=terminal;

/************************************
* Cleaning up
************************************/
system, "rm ps_repo";
system, "rm ps_lhc_repo";
stop;










