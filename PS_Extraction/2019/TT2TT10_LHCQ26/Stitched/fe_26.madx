
!--------------------------------------------------------------------------
! MADX file for TT2/TT10 optics calculations
! K.Hanke & M.Giovannozzi 1998, original version by G.Arduini
! Directory: /afs/cern.ch/eng/ps/cps/TransLines/PS-SPS/2011
! Converted to MADX June 2006 O.Berrig
! Execute with:  >mad < fe_26.madx > echo.pri
!
! This file is for protons at 26 GeV/c with QKE58 OFF inside PS
! Measurements done by E.Benedetto and S.Gilardoni on 13 June 2007
!
! References to noqke58 were removed, as this is now standard procedure
!--------------------------------------------------------------------------

 title, 'TT2/TT10 optics. Protons - 26 GeV/c';

 option, echo;
 option, RBARC=FALSE;  ! the length of a rectangular magnet
                       ! is the distance between the polefaces
                       ! and not the arc length


/*******************************************************************************
 * TT2
 * NB! The order of the .ele .str and .seq files matter.
 *     The reason is a >feature< of MADX
 *******************************************************************************/
 call, file = '../elements/tt2.ele';
 call, file = '../strength/tt2_fe_26.str';
 call, file = '../sequence/tt2.seq';
 call, file = '../aperture/tt2.dbx';


/*******************************************************************************
 * TT10
 *******************************************************************************/

 call, file = '../elements/tt10.ele';
 call, file = '../strength/tt10_fe_26.str';
 call, file = '../sequence/tt10.seq';
 call, file = '../aperture/tt10.dbx';


 /***************************************************
 * Files for SPS
 ***************************************************/

 call, file = '/afs/cern.ch/eng/sps/2014
               /elements/sps.ele';
 call, file = '/afs/cern.ch/eng/sps/2014
               /aperture/aperturedb_1.dbx';
 call, file = '/afs/cern.ch/eng/sps/2014
               /aperture/aperturedb_2.dbx';
 call, file = '/afs/cern.ch/eng/sps/2014
               /sequence/sps.seq';
 call, file = '/afs/cern.ch/eng/sps/2014
               /aperture/aperturedb_3.dbx';
 call, file = '/afs/cern.ch/eng/sps/2014
               /strength/ft_noqs_ext.str';
 call, file = '/afs/cern.ch/eng/sps/2014
               /strength/elements.str';
!option, echo;


/*******************************************************************************
 * Junction between TT10 and SPS
 *******************************************************************************/

 SEQEDIT, SEQUENCE=SPS;
 ENDTT10 : MARKER;
 install, element = ENDTT10, at = -0.1375, from = BPH.12008;
 ENDEDIT;

 SEQEDIT, SEQUENCE=SPS;
 ENDVKNV : MARKER;
 install, element = ENDVKNV, at = 10.3015+0.1755, from = BPCE.11833;
 ENDEDIT;

 SEQEDIT, SEQUENCE=SPS;
 flatten ; cycle, start=  ENDVKNV;
 ENDEDIT;


/*******************************************************************************
 * build up the geometry of the beam lines and select a line
 *******************************************************************************/

 tt2tt10: sequence, refer=ENTRY, l = 1164.8409;
  tt2a                  , at =        0;
  tt2b                  , at = 136.3114;
  tt2c                  , at = 249.9449;
  tt10                  , at = 304.6954;
 endsequence;

 tt2tt10sps: sequence, refer=ENTRY, l = 8076.3447;
  tt2a                  , at =        0;
  tt2b                  , at = 136.3114;
  tt2c                  , at = 249.9449;
  tt10                  , at = 304.6954;
  sps                   , at = 1164.8409;
 endsequence;




/*******************************************************************************
 * set initial twiss parameters
 *******************************************************************************/
 call, file = '../inp/tt2_fe_26.inp';



/*******************************************************************************
 * set initial position and angle (x,px)
 * E.g. :
 * x0 := 0.0;
 * px0:= 0.0;
 *******************************************************************************/




/*******************************************************************************
 * store initial parameters in memory block
 *******************************************************************************/
INITBETA0: BETA0,
  BETX=BETX0,
  ALFX=ALFX0,
  MUX=MUX0,
  BETY=BETY0,
  ALFY=ALFY0,
  MUY=MUY0,
  X=X0,
  PX=PX0,
  Y=Y0,
  PY=PY0,
  T=T0,
  PT=PT0,
  DX=DX0,
  DPX=DPX0,
  DY=DY0,
  DPY=DPY0;



/*******************************************************************************
 * save a sequence
 *******************************************************************************/
 !save, sequence=tt2tt10, file=tt2tt10.save;




/*******************************************************************************
 * beam & use
 *******************************************************************************/
! The energy spread (dp/p) is 1.0E-3
! NB!    The reason that the emittance is mutiplied by 4 i.e. exn=3.0E-6*4, is a MAD definition

 Beam, particle=PROTON,pc=26,exn=3.0E-6*4.0,eyn=3.0E-6*4.0;

 use, sequence=tt2tt10sps;

 !value, QDE210->K1;
 !show, QDE210;
 !show, dfa1kick;
 !stop;



/*******************************************************************************
 * maketwiss macro
 *******************************************************************************/
 maketwiss : macro={
!                   use, sequence=tt2tt10sps;
                    ptc_create_universe;
                    ptc_create_layout,model=2,method=6,nst=5,time=true,exact;
                    ptc_twiss, table=ptc_twiss, BETA0=INITBETA0, icase=5, no=1; ! , file="twiss"
                    ptc_end;
                   }



/*******************************************************************************
 * TWISS PTC
 *******************************************************************************/
 exec, maketwiss;

!------------------------ write ptc_twiss table -----------------
!set,  format="10.5f"; ! the format of numbers in the twiss output file
!
!select,flag=ptc_twiss,clear;
!select,flag=ptc_twiss, column =name,angle,k1l,k2l,k3l,beta11,beta22,disp1,disp3,
!                               x,y,alfa11,alfa22,mu1,mu2,disp2,disp4,px,py;
!write,table=ptc_twiss, file="../out/opticsxfe26.out";
!----------------------------------------------------------------


!------------------------ write ptc_twiss table -----------------
set,  format="10.5f"; ! the format of numbers in the twiss output file

select,flag=ptc_twiss,clear;
select,flag=ptc_twiss, column = name,s,x,px,disp1,dimxc,alfa11,beta11,mu1,
                                       y,py,disp3,dimyc,alfa22,beta22,mu2;
write,table=ptc_twiss, file="../out/opticsxfe26.out";
!----------------------------------------------------------------

value,table(ptc_twiss,TT2TT10SPS$END,beta12);




/*******************************************************************************
 * PLOT
 *******************************************************************************/

 /* Use >gv madx.ps to plot */

! option, -info;
!option, -echo;

 resplot;
 setplot, post=2;

 plot, title='FE_26' , table=ptc_twiss
                     , haxis=s
                     , vaxis1=beta11,beta22
                     , style:=100,symbol:=4,colour=100
                     , file = "../out/fe_26_";

 plot, title='Twiss' , table=ptc_twiss
                     , haxis=s
                     , vaxis1=beta11,beta22
                     , vaxis2=disp1
                     , range=#S/#e
                     , style:=100, symbol:=4, colour=100;

 plot, title='Twiss' , table=ptc_twiss
                     , haxis=s
                     , vaxis1=disp1,disp3
                     , range=#S/#e
                     , style:=100, symbol:=4, colour=100;


/*******************************************************************************
 * Some useful commands:
 *******************************************************************************/

!show, dfa1kick;

/*
Assign, echo="KICKER_STRENGTHX";
print, text="FAST BUMP";
show, KICKDFA242, KICKDFA254;
Assign, echo=terminal;
*/


stop;

